/**
 * View Models used by Spring MVC REST controllers.
 */
package com.andreihirvi.gateway.web.rest.vm;
