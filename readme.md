# Invoicing System for a Parking House

Each microservice is using JHipster

## Service (all microservices at once) is possible to deploy to Docker 
You need to navigate to docker-compose directory and run this:

      docker-compose up -d
      
For building image for each microservice you need to navigate to each microservice's directory and run this:

      ./mvnw -ntp -Pprod verify jib:dockerBuild


## Generate Invoice Endpoint (for testing)
    http://localhost:8080/services/vendorapi/api/invoice/1
## JHipster registry (Eureka server + Config server).

JHipster registry URL : http://localhost:8761

JHipster registry, Service Discovery, Config Server 

(without Docker) runs automatically with API Gateway


## API Gateway

API Gateway URL : http://localhost:8080

Back-office front-end (ReactJS), Zuul

To start gateway (without Docker), run (from directory): 

      ./mvnw
      
## Other Microservices

To run microservices without Docker you need to run each service from its directory: 
        
      ./mvnw