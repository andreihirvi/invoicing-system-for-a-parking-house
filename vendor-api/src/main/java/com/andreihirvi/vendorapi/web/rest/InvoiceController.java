package com.andreihirvi.vendorapi.web.rest;

import com.andreihirvi.vendorapi.domain.InvoiceDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
@RequestMapping("/api/invoice")
public class InvoiceController {

    @GetMapping("/{customerId}")
    public InvoiceDTO createInvoice(@PathVariable Long customerId) {
        ResponseEntity<InvoiceDTO> responseEntity =
            new RestTemplate().getForEntity(
                "http://localhost:8084/api/invoice/" + customerId, InvoiceDTO.class);
        return responseEntity.getBody();
    }
}
