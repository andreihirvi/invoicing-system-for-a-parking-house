/**
 * View Models used by Spring MVC REST controllers.
 */
package com.andreihirvi.vendorapi.web.rest.vm;
