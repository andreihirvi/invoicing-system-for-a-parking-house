package com.andreihirvi.vendorapi.domain;

public enum ProductType {
    MEMBER_FEE,
    PARKING
}
