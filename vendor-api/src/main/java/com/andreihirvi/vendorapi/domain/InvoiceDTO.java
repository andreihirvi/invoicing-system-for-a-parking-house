package com.andreihirvi.vendorapi.domain;


import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

@JsonDeserialize
public class InvoiceDTO {
    private Long customerID;
    private List<ParkingDTO> parkings;
    private String currency = "EUR";
    private Long totalAmount;

    public InvoiceDTO() { }

    public InvoiceDTO(Long customerID, List<ParkingDTO> parkings, Long totalAmount) {
        this.customerID = customerID;
        this.parkings = parkings;
        this.totalAmount = totalAmount;
    }


    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerID) {
        customerID = customerID;
    }


    public List<ParkingDTO> getParkings() {
        return parkings;
    }

    public void setParkings(List<ParkingDTO> parkings) {
        this.parkings = parkings;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Long totalAmount) {
        this.totalAmount = totalAmount;
    }

}
