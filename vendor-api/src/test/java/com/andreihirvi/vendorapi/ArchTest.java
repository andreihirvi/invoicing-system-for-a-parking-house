package com.andreihirvi.vendorapi;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.andreihirvi.vendorapi");

        noClasses()
            .that()
                .resideInAnyPackage("com.andreihirvi.vendorapi.service..")
            .or()
                .resideInAnyPackage("com.andreihirvi.vendorapi.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..com.andreihirvi.vendorapi.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
