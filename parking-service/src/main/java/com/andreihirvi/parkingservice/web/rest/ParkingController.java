package com.andreihirvi.parkingservice.web.rest;

import com.andreihirvi.parkingservice.domain.ProductType;
import com.andreihirvi.parkingservice.domain.ParkingDTO;
import com.andreihirvi.parkingservice.domain.VendorResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.*;

@RestController
@RequestMapping("/api/parking")
public class ParkingController {


    @GetMapping("/usage/{customerId}")
    public Map<String, List<ParkingDTO>> retrieveParkingInfo(@PathVariable Long customerId) {
        // Here should be API call to 3rd Party Vendor
        Instant parkingEndTime = Instant.now();
        Instant parkingStart = parkingEndTime.minusSeconds(60 * 60 * 2);
        VendorResponse emulatedResponse = new VendorResponse(customerId, 1L, parkingStart, parkingEndTime);
        // Try to get the same entity with same ExternalID to check duplicates
        // If entity not exists in DB, then save to DB

        return new HashMap<String, List<ParkingDTO>>() {{
            put("parkings", Arrays.asList(new ParkingDTO(
                emulatedResponse.getCustomerID(),
                emulatedResponse.getParkingID(),
                emulatedResponse.getStartTime(),
                emulatedResponse.getEndTime(),
                ProductType.PARKING)));
        }};
    }
}
