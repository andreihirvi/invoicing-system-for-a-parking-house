/**
 * View Models used by Spring MVC REST controllers.
 */
package com.andreihirvi.parkingservice.web.rest.vm;
