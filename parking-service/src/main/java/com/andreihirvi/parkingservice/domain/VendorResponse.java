package com.andreihirvi.parkingservice.domain;

import java.time.Instant;

public class VendorResponse {
    private Long customerID;
    private Long parkingID;
    private Instant startTime;
    private Instant endTime;

    public VendorResponse(Long customerID, Long parkingID, Instant startTime, Instant endTime) {
        this.customerID = customerID;
        this.parkingID = parkingID;
        this.startTime = startTime;
        this.endTime = endTime;
    }
    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerID) {
        customerID = customerID;
    }

    public Long getParkingID() {
        return parkingID;
    }

    public void setParkingID(Long parkingID) {
        this.parkingID = parkingID;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }


}
