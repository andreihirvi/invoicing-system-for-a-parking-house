package com.andreihirvi.parkingservice.domain;

public enum ProductType {
    MEMBER_FEE,
    PARKING
}
