# JHipster generated Docker-Compose configuration

## Usage

Launch all your infrastructure by running: `docker-compose up -d`.

## Configured Docker services

### Service registry and configuration server:

- [JHipster Registry](http://localhost:8761)

### Applications and dependencies:

- gateway (gateway application)
- gateway's no database
- invoiceservice (microservice application)
- invoiceservice's no database
- parkingservice (microservice application)
- parkingservice's no database
- vendorapi (microservice application)
- vendorapi's no database

### Additional Services:
