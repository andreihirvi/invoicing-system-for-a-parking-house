package com.andreihirvi.invoiceservice.web.rest;

import com.andreihirvi.invoiceservice.domain.InvoiceDTO;
import com.andreihirvi.invoiceservice.domain.ParkingDTO;
import com.andreihirvi.invoiceservice.domain.ParkingResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;


@RestController
@RequestMapping("/api/invoice")
public class InvoiceController {

    @GetMapping("/{customerId}")
    public InvoiceDTO createInvoice(@PathVariable Long customerId) {
        ResponseEntity<ParkingResponse> responseEntity =
            new RestTemplate().getForEntity(
                "http://localhost:8081/api/parking/usage/" + customerId, ParkingResponse.class);

        List<ParkingDTO> parkings = responseEntity.getBody().getParkings();

        long totalAmount = 1000;
        // Here should be totalAmount calculation logic
        // Here we need to save Invoice to database

        return new InvoiceDTO(customerId, parkings, totalAmount);
    }
}
