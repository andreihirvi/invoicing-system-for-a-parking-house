/**
 * View Models used by Spring MVC REST controllers.
 */
package com.andreihirvi.invoiceservice.web.rest.vm;
