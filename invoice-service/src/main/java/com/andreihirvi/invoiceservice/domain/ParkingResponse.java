package com.andreihirvi.invoiceservice.domain;

import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

@JsonDeserialize
public class ParkingResponse {

    private List<ParkingDTO> parkings;

    public ParkingResponse() {}

    public ParkingResponse(List<ParkingDTO> parkings) {
        this.parkings = parkings;
    }

    public List<ParkingDTO> getParkings() {
        return parkings;
    }

    public void setParkings(List<ParkingDTO> parkings) {
        this.parkings = parkings;
    }


}
