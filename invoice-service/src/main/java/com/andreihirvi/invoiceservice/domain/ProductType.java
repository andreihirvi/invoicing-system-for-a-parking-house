package com.andreihirvi.invoiceservice.domain;

public enum ProductType {
    MEMBER_FEE,
    PARKING
}
