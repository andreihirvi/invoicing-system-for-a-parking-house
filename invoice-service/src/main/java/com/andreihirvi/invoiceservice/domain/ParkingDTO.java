package com.andreihirvi.invoiceservice.domain;


import java.time.Instant;

public class ParkingDTO {
    private Long customerID;
    private Long parkingID;
    private Instant startTime;
    private Instant endTime;
    private ProductType productType;

    public ParkingDTO() { }

    public ParkingDTO(Long customerID, Long parkingID, Instant startTime, Instant endTime, ProductType productType) {
        this.customerID = customerID;
        this.parkingID = parkingID;
        this.startTime = startTime;
        this.endTime = endTime;
        this.productType = productType;
    }

    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerID) {
        customerID = customerID;
    }

    public Long getParkingID() {
        return parkingID;
    }

    public void setParkingID(Long parkingID) {
        this.parkingID = parkingID;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

}

